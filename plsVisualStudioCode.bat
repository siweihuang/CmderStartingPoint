@echo off
set ProjectFolder=%1
IF "%ProjectFolder%"=="common" ( start Code "C:\Projects\GitLab\nodejs-common-utils" ) ELSE ^
IF "%ProjectFolder%"=="frontend" ( start Code "C:\Projects\GitLab\frontend" ) ELSE ^
IF "%ProjectFolder%"=="" ( start Code ) ELSE ^
REM default case...
cls