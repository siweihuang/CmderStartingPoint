@echo off
set _test=123456789abcdef0
echo Main string:%_test%

echo Extract only the first character
set _result=%_test:~0,1%
echo %_result%

echo Extract only the first 5 characters
set _result=%_test:~0,5%
echo %_result%

echo Skip 7 characters and then extract the next 5
set _result=%_test:~7,5%
echo %_result%

echo Skip 7 characters and then extract everything else
set _result=%_test:~7%
echo %_result%

echo -------------------------------------
echo Date Realted
echo.Date   : %date%
echo.Weekday: %date:~11,3%
echo.Month  : %date:~3,2%
echo.Day    : %date:~0,2%
echo.Year   : %date:~6,4%


setlocal enabledelayedexpansion
set S=/name1/name2/name3
set I=0
set L=-1
:l
if "!S:~%I%,1!"=="" goto ld
if "!S:~%I%,1!"=="/" set L=%I%
set /a I+=1
goto l
:ld
echo %L%