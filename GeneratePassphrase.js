/** GeneratePassphrase.js */
'use strict'

writeln("------Start---------");
writeln("Please enter the plain text from keyboard:");
// var pw = "password"
var pw = readLine();

writeln("Input password is:" + pw);
if(pw == "" || pw.indexOf(" ") > -1 || pw.indexOf("\t") > -1){
    writeln("Empty text has been detected, exit program now");
} else {
    // Source String used when generating a random identifier.
    var s = new Stream('idSource.txt')
    const idSource = s.readFile();
    writeln(idSource);
    
    var keyArray = [];
    var finalPassphrase = "";
    var batchVariable = "";
    for (var i=0; i< pw.length; i++) {
        var currentLetter = pw[i];
    
        //Option 1 to populate keyArray
        var n = idSource.indexOf(currentLetter);
        keyArray.push(n);
        batchVariable += "%id:~" + n +",1%";
    }
    writeln(keyArray);
    writeln("Copy and paster the following to your *.bat file");
    writeln(batchVariable);
    
    for (var i in keyArray) {
        finalPassphrase += idSource[keyArray[i]];
    }
    
    writeln("Password revert back:" + finalPassphrase);
}

writeln("---------End---------");