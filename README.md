# cmder

> Using cmder for quick accessing Chrome, VisualStudioCode, Notepad, PuTTY, WinSCP and more

## Build Setup

``` bash
#Download cmder from the following url, recommend to download the Full Portable Version
https://cmder.net/

# From Windows work station, create a Root Folder, e.g.
C:\Projects\StartingPoint

# Open Chrome, if Chrome has been installed on your Windows work station
plsChrome.bat
plsChrome.bat http://yahoo.com

# Open Notepad
plsNotepad.bat

# Open current working directory
plsOpenCurrentFolder.bat

# Open Visual Studio Code and a particular project
plsVisualStudioCode.bat common
plsVisualStudioCode.bat frontend

# Open PuTTY.exe and ssh to a server, without entering password again and again. (Internal usage only)
plsPuTTY.bat 148
plsPuTTY.bat 132

# Open winSCP.exe and sftp to a server, without entering password again and again. (Internal usage only)
plsWinSCP.bat 148
plsWinSCP.bat 132

# Open WeChat, use exising Windows shortcut
WeChat.lnk

```

## Generate Pass Phrase

``` bash
#idSource.txt
Pre-populated idSource.txt contains the source string, which can only be entered from key board and in pre-defined sequence.
Keep this file to yourself. Not mean to be shared public.

#GeneratePassphrase.js
Command to execute this standalone JavaScript using (http://www.jsdb.org/):
jsdb.exe GeneratePassphrase.js <InputPassword>

#Using the pass phrase
The long string of ...%id:~28,1%%id:~25,1%... is the one that used in plsPuTTY.bat and plsWinSCP.bat
for passing in password in non-plain text format.

```

## JSDB (JavaScript for databases)

``` bash
JSDB is based on Mozilla SpiderMonkey and class libraries developed by Raosoft, Inc. The open-source version is free with fairly generous license terms. Additional functionality or even less restrictive license terms may be purchased from Raosoft, Inc.

You might use JSDB to:
  - translate XML into SQL statements,
- translate SQL databases into XML,
- generate HTML documentation from an XML or Oracle database,
- download web pages while you sleep,
- index their contents in a database,
- send personalized email based on a database,
- or run a web database

http://www.jsdb.org/
```